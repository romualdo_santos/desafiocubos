﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DesafioCubos.Data;
using DesafioCubos.Models;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace DesafioCubos.Controllers
{
    public class AlunoesController : Controller
    {
        private readonly DesafioCubosContext _context;

        public AlunoesController(DesafioCubosContext context)
        {
            _context = context;
        }

        // GET: Alunoes
        public async Task<IActionResult> Index()
        {
            var cursos = await _context.Aluno.ToListAsync();
            foreach (var item in cursos)
            {
                item.Curso = await _context.Curso.FindAsync(item.CursoID);
            }
            return View(cursos);
        }

        private void PopulateCursosDropDownList(object selectedCurso = null)
        {
            var cursosQuery = from c in _context.Curso.Where(x => x.QuantidadeDeVagas > 0)
                              orderby c.Nome
                              select c;
            ViewBag.CursoID = new SelectList(cursosQuery.AsNoTracking(), "ID", "Nome", selectedCurso);
        }

        // GET: Alunoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aluno = await _context.Aluno
                .FirstOrDefaultAsync(m => m.ID == id);
            if (aluno == null)
            {
                return NotFound();
            }
            aluno.Curso = await _context.Curso
                .FirstOrDefaultAsync(m => m.ID == aluno.CursoID);
            return View(aluno);
        }

        
        // GET: Alunoes/Create
        public IActionResult Create()
        {
            PopulateCursosDropDownList();
            return View();            
        }

        // POST: Alunoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Nome,NomeSocial,Email,Cpf,Endereco,CursoID,Ip")] Aluno aluno)
        {
            var curso = await _context.Curso.FindAsync(aluno.CursoID);            
            if (curso == null || curso.QuantidadeDeVagas == 0)
            {
                return View("Views/Alunoes/CursoErrors.cshtml");
            }

            //String cpfFormatado = aluno.Cpf.Trim('.').Trim('-');            
            //aluno.Cpf = cpfFormatado;

            var otherAluno = await _context.Aluno
                .FirstOrDefaultAsync(m => m.Cpf== aluno.Cpf && m.CursoID == aluno.CursoID);
            if(otherAluno != null)
            {
                return View("Views/Alunoes/JaMatriculadoError.cshtml");
            }

            if (ModelState.IsValid)
            {                
                //aluno.Cpf = cpfFormatado;
                _context.Add(aluno);
                curso.QuantidadeDeVagas--;
                _context.Update(curso);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            PopulateCursosDropDownList(aluno.CursoID);
            return View(aluno);
        }

        // GET: Alunoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aluno = await _context.Aluno.FindAsync(id);
            if (aluno == null)
            {
                return NotFound();
            }
            PopulateCursosDropDownList(aluno.CursoID);
            return View(aluno);
        }

        // POST: Alunoes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Nome,NomeSocial,Email,Cpf,Endereco,Ip")] Aluno aluno)
        {
            if (id != aluno.ID)
            {
                return NotFound();
            }

            var curso = await _context.Curso.FindAsync(aluno.CursoID);
            if (curso == null || curso.QuantidadeDeVagas == 0)
            {
                return View("Views/Alunoes/CursoErrors.cshtml");
            }

            //String cpfFormatado = aluno.Cpf.Trim('.').Trim('-');            
            //aluno.Cpf = cpfFormatado;

            var otherAluno = await _context.Aluno
                .FirstOrDefaultAsync(m => m.Cpf == aluno.Cpf && m.CursoID == aluno.CursoID);
            if (otherAluno != null)
            {
                return View("Views/Alunoes/JaMatriculadoError.cshtml");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(aluno);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AlunoExists(aluno.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }

            PopulateCursosDropDownList(aluno.CursoID);

            return View(aluno);
        }

        // GET: Alunoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aluno = await _context.Aluno
                .FirstOrDefaultAsync(m => m.ID == id);
            if (aluno == null)
            {
                return NotFound();
            }

            return View(aluno);
        }

        // POST: Alunoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {            
            var aluno = await _context.Aluno.FindAsync(id);
            var curso = await _context.Curso.FindAsync(aluno.CursoID);
            curso.QuantidadeDeVagas++;
            _context.Update(curso);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AlunoExists(int id)
        {
            return _context.Aluno.Any(e => e.ID == id);
        }
    }
}
