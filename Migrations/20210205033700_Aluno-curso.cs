﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DesafioCubos.Migrations
{
    public partial class Alunocurso : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Curso_Aluno_AlunoID",
                table: "Curso");

            migrationBuilder.DropIndex(
                name: "IX_Curso_AlunoID",
                table: "Curso");

            migrationBuilder.DropColumn(
                name: "AlunoID",
                table: "Curso");

            migrationBuilder.AddColumn<int>(
                name: "CursoID",
                table: "Aluno",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Aluno_CursoID",
                table: "Aluno",
                column: "CursoID");

            migrationBuilder.AddForeignKey(
                name: "FK_Aluno_Curso_CursoID",
                table: "Aluno",
                column: "CursoID",
                principalTable: "Curso",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Aluno_Curso_CursoID",
                table: "Aluno");

            migrationBuilder.DropIndex(
                name: "IX_Aluno_CursoID",
                table: "Aluno");

            migrationBuilder.DropColumn(
                name: "CursoID",
                table: "Aluno");

            migrationBuilder.AddColumn<int>(
                name: "AlunoID",
                table: "Curso",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Curso_AlunoID",
                table: "Curso",
                column: "AlunoID");

            migrationBuilder.AddForeignKey(
                name: "FK_Curso_Aluno_AlunoID",
                table: "Curso",
                column: "AlunoID",
                principalTable: "Aluno",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
