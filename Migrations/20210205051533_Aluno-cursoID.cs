﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DesafioCubos.Migrations
{
    public partial class AlunocursoID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Aluno_Curso_CursoID",
                table: "Aluno");

            migrationBuilder.AlterColumn<int>(
                name: "CursoID",
                table: "Aluno",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "Teste",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CursoID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teste", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Teste_Curso_CursoID",
                        column: x => x.CursoID,
                        principalTable: "Curso",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Teste_CursoID",
                table: "Teste",
                column: "CursoID");

            migrationBuilder.AddForeignKey(
                name: "FK_Aluno_Curso_CursoID",
                table: "Aluno",
                column: "CursoID",
                principalTable: "Curso",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Aluno_Curso_CursoID",
                table: "Aluno");

            migrationBuilder.DropTable(
                name: "Teste");

            migrationBuilder.AlterColumn<int>(
                name: "CursoID",
                table: "Aluno",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Aluno_Curso_CursoID",
                table: "Aluno",
                column: "CursoID",
                principalTable: "Curso",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
