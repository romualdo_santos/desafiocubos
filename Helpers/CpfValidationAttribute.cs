﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Globalization;
using System.Text.RegularExpressions;

namespace DesafioCubos.Helpers
{
    public class CpfValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string info;
            string ok;
            int score;

            try
            {
                string auxInfo = new System.Net.WebClient().DownloadString("https://api.cubos.io/psel/score/" + value);
                info = auxInfo;
            }
            catch (WebException)
            {
                info = "";
            }

            Regex regex = new Regex(@"\Wok\W\s?\:\s?(?<ok>\w+).+\Wscore\W\s?\:\s?(?<score>\d+).");
            Match m = regex.Match(info);

            if (m.Success)
            {
                ok = m.Groups["ok"].Value;
                score = Int32.Parse(m.Groups["score"].Value);
                if (ok == "true" && score >= 300)
                {
                    return ValidationResult.Success;
                }

                else {
                    return new ValidationResult("CPF com score baixo");
                }
            }

            return new ValidationResult("CPF inválido");
        }
    }
}
