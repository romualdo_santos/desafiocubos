﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Net.Sockets;
using System.Net;

namespace DesafioCubos.Helpers
{
    public class IpRegionValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {           
            
            IpInfo ipInfo = new IpInfo();
            try
            {
                string info = new System.Net.WebClient().DownloadString("http://ipinfo.io/" + value);
                ipInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<IpInfo>(info);
                RegionInfo myRI1 = new RegionInfo(ipInfo.Country);
                ipInfo.Country = myRI1.EnglishName;
            }
            catch (Exception)
            {
                ipInfo.Country = null;
            }

            //VerifyIp();
            if (ipInfo.Country == "Brazil")
            {
                return ValidationResult.Success;
            }

            return new ValidationResult("Ip inválido");
        }
    }
}
