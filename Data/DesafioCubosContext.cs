﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DesafioCubos.Models;

namespace DesafioCubos.Data
{
    public class DesafioCubosContext : DbContext
    {
        public DesafioCubosContext (DbContextOptions<DesafioCubosContext> options)
            : base(options)
        {
        }

        public DbSet<DesafioCubos.Models.Curso> Curso { get; set; }

        public DbSet<DesafioCubos.Models.Aluno> Aluno { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Curso>().ToTable("Curso");            
            modelBuilder.Entity<Aluno>().ToTable("Aluno");
        }

        public DbSet<DesafioCubos.Models.Teste> Teste { get; set; }
    }
}
