﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioCubos.Models
{
    public class Curso
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Categoria { get; set; }
        public string Descricao { get; set; }
        public int QuantidadeDeVagas { get; set; }
    }
}
