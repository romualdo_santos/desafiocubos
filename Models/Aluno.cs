﻿using DesafioCubos.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DesafioCubos.Models
{
    public class Aluno
    {
        public int ID { get; set; }
        [Required]
        public string Nome { get; set; }

        [Display(Name = "Nome Social")]
        public string NomeSocial { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        [CpfValidation]
        public string Cpf { get { return _cpf.Trim('.').Trim('-'); } set { _cpf = value.Trim('.').Trim('-'); }}
        private string _cpf;

        public string Endereco { get; set; }

        [Required]
        [IpRegionValidation]
        public string Ip { get; set; }        

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CursoID { get; set; }
        public Curso Curso { get; set; }

        public static string Address
        {
            get
            {
                {
                    String address = "";
                    WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
                    using (WebResponse response = request.GetResponse())
                    using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                    {
                        address = stream.ReadToEnd();
                    }

                    int first = address.IndexOf("Address: ") + 9;
                    int last = address.LastIndexOf("</body>");
                    address = address.Substring(first, last - first);
                    return address;
                }
            }
        }
    }
}
